import React from 'react';


class ConferenceForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            maxPresentations: '',
            maxAttendees: '',
            locations: []
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartsChange = this.handleStartsChange.bind(this);
        this.handleEndsChange = this.handleEndsChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees;
        delete data.maxPresentations;
        delete data.maxAttendees;
        delete data.locations;
        

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);

        if (response.ok) {
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                maxPresentations: '',
                maxAttendees: '',
                location: '',
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value});
    }

    handleStartsChange(event) {
        const value = event.target.value;
        this.setState({starts: value});
    }

    handleEndsChange(event) {
        const value = event.target.value;
        this.setState({ends: value});
    }

    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value});
    }

    handleMaxPresentationsChange(event) {
        const value = event.target.value;
        this.setState({maxPresentations: value});
    }

    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({maxAttendees: value});
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value});
    }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} name="name" placeholder="Name" required
                        type="text" id="name" value={this.state.name}
                        className="form-control" />
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleStartsChange} name="starts" placeholder="Starts" required
                        type="date" id="starts" value={this.state.starts}
                        className="form-control" />
                    <label htmlFor="starts">Starts</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEndsChange} name="ends" placeholder="Ends" required 
                        type="date" id="ends" value={this.state.ends}
                        className="form-control" />
                    <label htmlFor="room_count">Ends</label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="description" className="form-label">Description</label>
                    <textarea onChange={this.handleDescriptionChange} value={this.state.description} className="form-control" 
                        id="description" name="description" rows="3"></textarea>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleMaxPresentationsChange} name="max_presentations" placeholder="Maximum presentations" 
                        required type="number" id="max_presentations" value={this.state.maxPresentations}
                        className="form-control" />
                    <label htmlFor="max-presentations">Maximum presentations</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleMaxAttendeesChange} name="max_attendees" placeholder="Maximum attendees" 
                        required type="number" id="max_attendees" value={this.state.maxAttendees}
                        className="form-control" />
                    <label htmlFor="max-attendees">Maximum attendees</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleLocationChange} name="location" required id="location" 
                        value={this.state.location} className="form-select">
                    <option value="">Choose a location</option>
                    {this.state.locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                        );
                    })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
    }
}

export default ConferenceForm;